package jenica.lee.guerrero.weather.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="weather_log")
public class WeatherLog {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Column(name="response_id")
    private String responseId;

    @Column(name="location")
    private String location;

    @Column(name="actual_weather")
    private String actualWeather;

    @Column(name="temperature")
    private String temperature;

    @Column(name="date_time_inserted")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtimeInserted;

    public WeatherLog() {}

    public WeatherLog(Long id, String responseId, String location, String actualWeather, String temperature, Date dtimeInserted) {
        this.id = id;
        this.responseId = responseId;
        this.location = location;
        this.actualWeather = actualWeather;
        this.temperature = temperature;
        this.dtimeInserted = dtimeInserted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getActualWeather() {
        return actualWeather;
    }

    public void setActualWeather(String actualWeather) {
        this.actualWeather = actualWeather;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public Date getDtimeInserted() {
        return dtimeInserted;
    }

    public void setDtimeInserted(Date dtimeInserted) {
        this.dtimeInserted = dtimeInserted;
    }

}
