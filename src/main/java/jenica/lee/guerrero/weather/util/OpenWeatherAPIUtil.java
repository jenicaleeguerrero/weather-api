package jenica.lee.guerrero.weather.util;

import jenica.lee.guerrero.weather.service.impl.WeatherServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class OpenWeatherAPIUtil {

    private static final Logger log = LoggerFactory.getLogger(OpenWeatherAPIUtil.class);

    @Value("${server.open.weather.path}")
    private String OPEN_WEATHER_API_URL;

    @Value("${open.weather.api.key}")
    private String OPEN_WEATHER_API_KEY;

    @Autowired
    RestTemplate restTemplate;

    /**
     * Builds URL with given parameters
     * @param param city name
     * @return
     */
    public String buildUrl(String param) {
        return UriComponentsBuilder.fromHttpUrl(OPEN_WEATHER_API_URL)
                .queryParam("q", param)
                .queryParam("APPID", OPEN_WEATHER_API_KEY).toUriString();
    }

    /**
     * Calls Open Weather API
     * @param url
     * @return
     */
    public ResponseEntity<String> call(String url) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> entity = new HttpEntity<>(headers);

        return restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
    }
}
