package jenica.lee.guerrero.weather.controller;

import jenica.lee.guerrero.weather.dto.WeatherInfo;
import jenica.lee.guerrero.weather.dto.request.WeatherLogRequest;
import jenica.lee.guerrero.weather.dto.response.WeatherInfoResponse;
import jenica.lee.guerrero.weather.dto.response.WeatherLogResponse;
import jenica.lee.guerrero.weather.model.WeatherLog;
import jenica.lee.guerrero.weather.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/weather")
public class WeatherRestController {

    @Autowired
    WeatherService weatherService;


    @RequestMapping(method=RequestMethod.GET, value = "/{cities}", produces = "application/json")
    public ResponseEntity<?> getWeatherDetailsByCity(@PathVariable(value = "cities") List<String> cities) {
        WeatherInfoResponse weatherInfoResponse = new WeatherInfoResponse();

        List<WeatherInfo> weatherInfos = new ArrayList<>();
        for(String city : cities) {
            if(!"".equals(city)) {
                weatherInfos.add(weatherService.getWeatherInformationByCity(city));
            }
        }
        weatherInfoResponse.setWeatherInfo(weatherInfos);
        weatherInfoResponse.setMessage("SUCCESS");
        weatherInfoResponse.setStatus(HttpStatus.OK.value());
        return new ResponseEntity<>(weatherInfoResponse, HttpStatus.OK);
    }



    @RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<?> storeWeatherLogByCity(@RequestBody WeatherLogRequest weatherLogRequest) {
        WeatherLogResponse weatherLogResponse = new WeatherLogResponse();
        List<WeatherLog> weatherLogs = new ArrayList<>();
        for(String city : weatherLogRequest.getCities()) {
            weatherLogs.add(weatherService.storeWeatherInformation(city));
        }
        weatherLogResponse.setWeatherLogs(weatherLogs);
        weatherLogResponse.setMessage("SUCCESS");
        weatherLogResponse.setStatus(HttpStatus.OK.value());
        return new ResponseEntity<>(weatherLogResponse, HttpStatus.OK);
    }


}
