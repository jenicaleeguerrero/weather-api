package jenica.lee.guerrero.weather.dto.response;

import jenica.lee.guerrero.weather.dto.WeatherInfo;

import java.util.List;

public class WeatherInfoResponse extends BaseAPIResponse {

    private List<WeatherInfo> weatherInfo;

    public List<WeatherInfo> getWeatherInfo() {
        return weatherInfo;
    }

    public void setWeatherInfo(List<WeatherInfo> weatherInfo) {
        this.weatherInfo = weatherInfo;
    }
}
