package jenica.lee.guerrero.weather.dto.response;

import jenica.lee.guerrero.weather.model.WeatherLog;

import java.util.List;

public class WeatherLogResponse extends BaseAPIResponse {

    private List<WeatherLog> weatherLogs;

    public List<WeatherLog> getWeatherLogs() {
        return weatherLogs;
    }

    public void setWeatherLogs(List<WeatherLog> weatherLogs) {
        this.weatherLogs = weatherLogs;
    }
}
