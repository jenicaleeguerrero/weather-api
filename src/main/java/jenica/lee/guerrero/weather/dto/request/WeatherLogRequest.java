package jenica.lee.guerrero.weather.dto.request;

import java.util.List;

public class WeatherLogRequest {

    private List<String> cities;

    public List<String> getCities() {
        return cities;
    }

    public void setCities(List<String> cities) {
        this.cities = cities;
    }
}
