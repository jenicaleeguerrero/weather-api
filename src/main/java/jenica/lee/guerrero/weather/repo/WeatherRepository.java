package jenica.lee.guerrero.weather.repo;

import jenica.lee.guerrero.weather.model.WeatherLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WeatherRepository extends JpaRepository<WeatherLog, Long> {


}
