package jenica.lee.guerrero.weather.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jenica.lee.guerrero.weather.dto.WeatherInfo;
import jenica.lee.guerrero.weather.model.WeatherLog;
import jenica.lee.guerrero.weather.repo.WeatherRepository;
import jenica.lee.guerrero.weather.service.WeatherService;
import jenica.lee.guerrero.weather.util.OpenWeatherAPIUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;

@Service
public class WeatherServiceImpl implements WeatherService {

    @Autowired
    WeatherRepository weatherRepository;

    @Autowired
    OpenWeatherAPIUtil openWeatherAPIUtil;

    @Override
    public WeatherInfo getWeatherInformationByCity(String city) {
        return this.getWeatherDetails(openWeatherAPIUtil.call(openWeatherAPIUtil.buildUrl(city)));
    }

    @Override
    public WeatherLog storeWeatherInformation(String city) {
        WeatherLog weatherLog = weatherRepository.save(this.getWeatherLog(openWeatherAPIUtil.call(openWeatherAPIUtil.buildUrl(city))));
        return weatherLog;
    }


    /**
     * Helper method for generating weather info object
     * @param response
     * @return
     */
    private WeatherInfo getWeatherDetails(ResponseEntity<String> response) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = null;

        try {
            root = mapper.readTree(response.getBody());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String weatherMain = this.removeDoubleQuotes(String.valueOf(root.path("weather").get(0).path("main")));
        String weatherDesc = this.removeDoubleQuotes(String.valueOf(root.path("weather").get(0).path("description")));

        WeatherInfo weatherInfo = new WeatherInfo();
        weatherInfo.setActualWeather(weatherMain + " - " + weatherDesc);
        weatherInfo.setTemperature(String.valueOf(root.path("main").path("temp")));
        weatherInfo.setLocation(this.removeDoubleQuotes(String.valueOf(root.path("name"))));

        return weatherInfo;
    }

    /**
     * Helper method for generating weather log object
     * @param response
     * @return
     */
    private WeatherLog getWeatherLog(ResponseEntity<String> response) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = null;

        try {
            root = mapper.readTree(response.getBody());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String weatherMain = this.removeDoubleQuotes(String.valueOf(root.path("weather").get(0).path("main")));
        String weatherDesc = this.removeDoubleQuotes(String.valueOf(root.path("weather").get(0).path("description")));

        WeatherLog weatherLog = new WeatherLog();
        weatherLog.setResponseId(String.valueOf(root.path("id")));
        weatherLog.setLocation(this.removeDoubleQuotes(String.valueOf(root.path("name"))));
        weatherLog.setActualWeather(weatherMain + " - " + weatherDesc);
        weatherLog.setTemperature(String.valueOf(root.path("main").path("temp")));
        weatherLog.setDtimeInserted(new Date());

        return weatherLog;
    }

    /**
     * Helper method for removing double quotes from JSON value
     * @param value
     * @return
     */
    private String removeDoubleQuotes(String value) {
        return value.replaceAll("\"", "");
    }

}
