package jenica.lee.guerrero.weather.service;


import jenica.lee.guerrero.weather.dto.WeatherInfo;
import jenica.lee.guerrero.weather.model.WeatherLog;

public interface WeatherService {

    /**
     * Get weather information from openweathermap.org/api
     * Locations: London, Prague and San Francisco
     * @param city
     * @return
     */
    WeatherInfo getWeatherInformationByCity(String city);

    /**
     * Store weather information from openweathermap.org/api
     * @return
     */
    WeatherLog storeWeatherInformation(String city);

}
